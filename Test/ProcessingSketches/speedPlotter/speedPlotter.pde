import processing.serial.*;

  Serial myPortHallTest;        
  int xPos = 1;       
  float inByte = 0;

  void setup () {
    size(400, 300);
    println(Serial.list());
    myPortHallTest = new Serial(this, Serial.list()[0], 9600);
    myPortHallTest.bufferUntil('\n');
    background(0);
  }

  void draw () {
    stroke(127, 3, 0);
    line(xPos, height+20, xPos, height - inByte);
    if (xPos >= width) {
      xPos = 0;
      background(0);
    } else {
      xPos++;
    }
  }

  void serialEvent (Serial myPortHallTest) {
    String inString = myPortHallTest.readStringUntil('\n');

    if (inString != null) {
      inString = trim(inString);
      inByte = float(inString);
      println(inByte);
      inByte = map(inByte, 0, 1023, 0, height);
    }
  }