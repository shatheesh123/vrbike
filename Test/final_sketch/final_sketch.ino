
#include <EEPROM.h> 
#include <NewPing.h>

#define TRIGGER_PIN1  3 
#define ECHO_PIN1     4  
#define TRIGGER_PIN2  5
#define ECHO_PIN2 6
#define MAX_DISTANCE 100 
#define SONAR_NUM 2
#define hall_sensor 2

NewPing sonar[SONAR_NUM]={NewPing(TRIGGER_PIN1,ECHO_PIN1,MAX_DISTANCE),NewPing(TRIGGER_PIN2,ECHO_PIN2,MAX_DISTANCE)};
int hallState=0;
unsigned int min_speed = 0;
unsigned int max_speed = 40;
unsigned int dirTestSonar1;
unsigned int dirTestSonar2;


volatile unsigned long lastturn, time_press;
unsigned long previousMillis=0;
volatile float SPEED;
volatile char DIRECTIONVALUE='S';
volatile int SPEEDLEVEL=0;
volatile float DIST;
volatile boolean eeprom_flag;
float w_length = 2.050;
boolean flag;
boolean state, button;

void setup(){
  Serial.begin(115200);
  pinMode(hall_sensor,INPUT);
  attachInterrupt(0,hallSense,RISING);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(TRIGGER_PIN1, OUTPUT);
  pinMode(ECHO_PIN1, INPUT);
  pinMode(TRIGGER_PIN2, OUTPUT);
  pinMode(ECHO_PIN2, INPUT);
  DIST=(float)EEPROM.read(0)/10.0;
}

void hallSense(){
   if (millis() - lastturn > 50) { 
    digitalWrite(LED_BUILTIN, HIGH);
    SPEED = w_length / ((float)(millis() - lastturn) / 1000) * 3.6;  
    lastturn = millis();                                             
    DIST = DIST + w_length / 1000;                                   
  }
  eeprom_flag = 1;
}

void sonarDirectionSelect(){
  int duration1, distance1,duration2, distance2;
  digitalWrite (TRIGGER_PIN1, HIGH);
  delay(10);
  digitalWrite (TRIGGER_PIN1, LOW);
  duration1 = pulseIn (ECHO_PIN1, HIGH);
  distance1 = (duration1/2) / 29.1;
  dirTestSonar1=distance1;
  digitalWrite (TRIGGER_PIN2, HIGH);
  delay(10);
  digitalWrite (TRIGGER_PIN2, LOW);
  duration2 = pulseIn (ECHO_PIN2, HIGH);
  distance2 = (duration2/2) / 29.1;
  dirTestSonar2=distance2;
  if((78<=distance1 && distance1<82) && (78<=distance2 && distance2<82)){
    DIRECTIONVALUE='S';
  }
  if((74<=distance1 && distance1<78) && (82<=distance2 && distance2<84)){
    DIRECTIONVALUE='R';
  }
  if((83<=distance1 && distance1<86) && (75<=distance2 && distance2<78)){
    DIRECTIONVALUE='L';
  }
}

void serialPrint(){
  Serial.print(DIRECTIONVALUE);
//speedSelection(SPEED);
  Serial.print(" sonar1= ");
  Serial.print(dirTestSonar1);
  Serial.print(" sonar2= ");
  Serial.print(dirTestSonar2);
  Serial.print(" speed=");
  speedSelection(SPEED);
  Serial.print(SPEED);
  Serial.print(" level");
  Serial.print(SPEEDLEVEL);
  Serial.print('\n');
}

void speedSelection(float SPEED){
  if(SPEED<=5){
    SPEEDLEVEL=1;
  }
  if(5<SPEED && SPEED<=10){
    SPEEDLEVEL=2;
  }
  if(10<SPEED && SPEED<=15){
    SPEEDLEVEL=3;
  }
  if(15<SPEED && SPEED<=20){
    SPEEDLEVEL=4;
  }
  if(20<SPEED){
    SPEEDLEVEL=5;
  }
}

void loop(){

serialPrint();
sonarDirectionSelect();
//sonarLoop();

if ((millis() - lastturn) > 2000) {       
    SPEED = 0; 
    digitalWrite(LED_BUILTIN, LOW);                            
    if (eeprom_flag) {                      
      EEPROM.write(0, (float)DIST * 10.0); 
      eeprom_flag = 0;                      
    }
  }
}





