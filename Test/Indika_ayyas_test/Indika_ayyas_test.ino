
#include <EEPROM.h> 
#include <NewPing.h>

#define TRIGGER_PIN1  3 
#define ECHO_PIN1     4  
#define TRIGGER_PIN2  5
#define ECHO_PIN2 6
#define MAX_DISTANCE 100 
#define SONAR_NUM 2
#define hall_sensor 2

NewPing sonar[SONAR_NUM]=(NewPing(TRIGGER_PIN1, ECHO_PIN1, MAX_DISTANCE),NewPing(TRIGGER_PIN2, ECHO_PIN2, MAX_DISTANCE)); 

int hallState=0;
unsigned int min_speed = 0;
unsigned int max_speed = 40;


volatile unsigned long lastturn, time_press;
unsigned long previousMillis=0;
unsigned long interval=100;
volatile float SPEED;
volatile float DIST;
volatile boolean eeprom_flag;
float w_length = 2.050;
boolean flag;
boolean state, button;

void setup(){
  Serial.begin(115200);
  pinMode(hall_sensor,INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  attachInterrupt(0,hallSense,RISING);
  DIST=(float)EEPROM.read(0)/10.0;
}

void hallSense(){
   if (millis() - lastturn > 50) { 
    digitalWrite(LED_BUILTIN, HIGH);
    SPEED = w_length / ((float)(millis() - lastturn) / 1000) * 3.6;  
    lastturn = millis();                                             
    DIST = DIST + w_length / 1000;                                    
  }
  eeprom_flag = 1;
}

void loop(){
  serialPrint();
//  sonarDirectionSelect();
//   if ((millis() - lastturn) > 2000) {       
//    SPEED = 0; 
//    digitalWrite(LED_BUILTIN, LOW);                            
//    if (eeprom_flag) {                      
//      EEPROM.write(0, (float)DIST * 10.0); 
//      eeprom_flag = 0;                      
//    }
//  }
//  if ((unsigned long)(millis() - previousMillis) >= interval) {
//    previousMillis = millis();
//    Serial.println("thread2");
//   }
//     for (uint8_t i = 0; i < SONAR_NUM; i++) {
//      delay(50);
//      Serial.print(i);
//      Serial.print("=");
//      Serial.print(sonar[i].ping_cm());
//      Serial.print("cm ");
//}
}

void speedSelection(){
  if(SPEED<10){
  }
}

void sonarDirectionSelect(){
  int duration1, distance1,duration2, distance2;
  digitalWrite (TRIGGER_PIN1, HIGH);
  delayMicroseconds (1000);
  digitalWrite (TRIGGER_PIN1, LOW);
  duration1 = pulseIn (ECHO_PIN1, HIGH);
  distance1 = (duration1/2) / 29.1;
  Serial.println(distance1);
  digitalWrite (TRIGGER_PIN2, HIGH);
  delayMicroseconds (1000);
  digitalWrite (TRIGGER_PIN2, LOW);
  duration2 = pulseIn (ECHO_PIN2, HIGH);
  distance2 = (duration2/2) / 29.1;
  Serial.println(distance2);
}

void serialPrint(){
  Serial.print("S");
  Serial.print(" ");
  Serial.print("0");
  Serial.print(" ");
  Serial.print(SPEED);
  Serial.println(" ");
}


