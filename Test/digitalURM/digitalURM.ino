int trigPin = 3;
int echoPin = 4;
int trigPin2 = 5;
int echoPin2 = 6;

void setup() {
  Serial.begin (115200);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2, INPUT);
}

void loop(){
  sonarDirectionSelect();
}

void sonarDirectionSelect(){
  int duration1,distance1,duration2, distance2;
  digitalWrite (trigPin, HIGH);
  digitalWrite (trigPin, LOW);
  duration1 = pulseIn (echoPin, HIGH);
  distance1 = (duration1/2) / 29.1;
  Serial.println(distance1);
  digitalWrite (trigPin2, HIGH);
  digitalWrite (trigPin2, LOW);
  duration2 = pulseIn (echoPin2, HIGH);
  distance2 = (duration2/2) / 29.1;
  Serial.println(distance2);
}
