#include <NewPing.h>

#define TRIGGER_PIN1  1 
#define ECHO_PIN1     0  
#define TRIGGER_PIN2  18
#define ECHO_PIN2 19
#define MAX_DISTANCE 100 
#define SONAR_NUM 2

NewPing sonar[SONAR_NUM]=(NewPing(TRIGGER_PIN1, ECHO_PIN1, MAX_DISTANCE),NewPing(TRIGGER_PIN2, ECHO_PIN2, MAX_DISTANCE)); 

void setup() {
  Serial.begin(115200); 
}

void loop() {
 for (uint8_t i = 0; i < SONAR_NUM; i++) {
    delay(50);
    Serial.print(i);
    Serial.print("=");
    Serial.print(sonar[i].ping_cm());
    Serial.print("cm ");
  }
  Serial.println();
}
